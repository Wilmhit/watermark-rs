use clap::{App, Arg, SubCommand, crate_authors};
use image::{RgbImage, open};
use std::io::prelude::*;
use std::fs::OpenOptions;

mod processing;

fn main() {
    let matches = App::new("watermark")
        .author(crate_authors!("\n"))
        .arg(
            Arg::with_name("output")
                .short("o")
                .takes_value(true)
        )
        .subcommand(
            SubCommand::with_name("size")
                .about("Mesures how many bytes picture can store")
                .arg(
                    Arg::with_name("FILE")
                        .required(true)
                        .takes_value(true)
                )
        )
        .subcommand(
            SubCommand::with_name("mark")
                .about("Make picture store data")
                .arg(
                    Arg::with_name("PICTURE")
                        .required(true)
                        .takes_value(true)
                )
                .arg(
                    Arg::with_name("DATA")
                        .required(true)
                        .takes_value(true)
                )
        )
        .subcommand(
            SubCommand::with_name("read")
                .about("Read data stored in picture")
                .arg(
                    Arg::with_name("PICTURE")
                        .required(true)
                        .takes_value(true)
                )
        )
        .get_matches();

    let output_filename: Option<&str> = matches.value_of("output");

    if let Some(matches) = matches.subcommand_matches("read") {
        let picture = matches.value_of("PICTURE").unwrap();
        match output_filename {
            Some(out) => read(picture, out),
            None => read(picture, format!("{}.out", picture).as_str())
        };
    }
    if let Some(matches) = matches.subcommand_matches("mark") {
        let picture_filename = matches.value_of("PICTURE").unwrap();
        let data_filename = matches.value_of("DATA").unwrap();
        match output_filename { 
            Some(out) => mark(picture_filename, data_filename, out),
            None => mark(picture_filename, data_filename, picture_filename)
        };
    }
    if let Some(matches) = matches.subcommand_matches("size") {
        let image = open(matches.value_of("FILE").unwrap()).unwrap().into_rgb8();
        println!("This image can store {} bytes", get_available_space(&image));
    }
}

fn get_available_space(image: &RgbImage) -> u64 {
    let (x,y) = image.dimensions();
    return ((x as u128)*(y as u128)*3/8) as u64;
}

fn mark(picture_filename: &str, data_filename: &str, output_filename: &str) {
    let mut data_file = OpenOptions::new()
        .read(true)
        .open(data_filename)
        .expect("Could not read data file");
    let picture = open(&picture_filename)
        .expect("Could not read picture file")
        .into_rgb8();
    if data_file.metadata().unwrap().len() > get_available_space(&picture) {
        panic!("This picture cannot store this much data");
    }
    let mut data = Vec::new();
    data_file.read_to_end(&mut data).unwrap();
    processing::imprint(data, &picture).save(output_filename).unwrap();
}

fn read(picture_filename: &str, output_filename: &str) {
    let picture = open(&picture_filename)
        .expect("Could not read picture file")
        .into_rgb8();
    let data = processing::read_data(&picture);
    let mut data_file = OpenOptions::new()
        .write(true)
        .create(true)
        .open(output_filename)
        .expect("Could not write to file");
    data_file.write(data.as_slice()).unwrap();
}