use image::{RgbImage, Pixel};
use genawaiter::{yield_, rc::gen};
use bit::BitIndex;

fn split_bits(data: Vec<u8>) -> impl Iterator<Item = bool> {
    gen!({
        for byte in data.iter() {
            for x in (0..8).rev() {
                yield_!(byte.bit(x));
            }
        }
    }).into_iter()
}

pub fn imprint(data: Vec<u8>, original: &RgbImage) -> RgbImage {
    let mut bit_iter = split_bits(data);
    let mut image = original.clone();
    'outer_loop: for pixel in image.pixels_mut() {
        for color in pixel.channels_mut() {
            if let Some(bit) = bit_iter.next() {
                color.set_bit(0, bit);
            }
            else {
                break 'outer_loop;
            }
        }
    }
    image
}

pub fn read_data(image: &RgbImage) -> Vec<u8> {
    let mut booleans: Vec<bool> = Vec::new();
    for pixel in image.pixels() {
        for color in pixel.channels() {
            booleans.push(color.bit(0));
        }
    }
    let mut data: Vec<u8> = Vec::new();
    for x in (0..booleans.len()).step_by(8) {
        let mut byte:u8 = 0;
        if x+7 < booleans.len() {
            for offset in 0..8 {
                byte.set_bit(7-offset, booleans[x+offset]);
            }
        }
        data.push(byte);
    }
    data
}